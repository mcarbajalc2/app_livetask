import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { OwlDateTimeModule, OwlNativeDateTimeModule, OwlDateTimeIntl} from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CookieService } from 'angular2-cookie/services/cookies.service';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { LoginComponent } from './login/login.component';
import { HeaderFullComponent } from './general/header-full/header-full.component';

import { MiscService } from './misc.service';
import { LogService } from './log.service';

import { FirebaseService } from './firebase.service';



import { AuthService } from './auth.service';
import { MainComponent } from './main/main.component';
import { HeaderComponent } from './general/header/header.component';
import { ProfileBlockComponent } from './general/profile-block/profile-block.component';
import { MyTasksComponent } from './my-tasks/my-tasks.component';
import { InboxComponent } from './inbox/inbox.component';
import { TracingComponent } from './tracing/tracing.component';
import { ProfileService } from './profile.service';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { FolderService } from './folder.service';
import { TaskService } from './task.service';
import { FormsModule } from '@angular/forms';
import { SelectDialogComponent } from './general/select-dialog/select-dialog.component';
import { StoreComponent } from './store/store.component';
import { ExtensionService } from './extension.service';
import { StoreDetailComponent } from './store-detail/store-detail.component';
import { ExtensionComponent } from './extension/extension.component';
import { FeedBackComponent } from './feed-back/feed-back.component';
import { ProfileComponent } from './profile/profile.component';

export const DefaultIntl = {
  cancelBtnLabel: 'Cancelar',
  setBtnLabel: 'Aceptar',
  hour12AMLabel: 'AM',
  hour12PMLabel: 'PM',
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderFullComponent,
    MainComponent,
    HeaderComponent,
    ProfileBlockComponent,
    MyTasksComponent,
    InboxComponent,
    TracingComponent,
    TaskListComponent,
    TaskDetailComponent,
    SelectDialogComponent,
    StoreComponent,
    StoreDetailComponent,
    ExtensionComponent,
    FeedBackComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,
  ],
  providers: [
    AuthService,
    MiscService,
    LogService,
    CookieService,
    ProfileService,
    FolderService,
    TaskService,
    {provide: OwlDateTimeIntl, useValue: DefaultIntl},
    ExtensionService,
    FirebaseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
