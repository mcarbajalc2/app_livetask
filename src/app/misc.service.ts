import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class MiscService {

	private base_url = 'https://us-central1-livetask-363bb.cloudfunctions.net/api/';

  constructor(private http: HttpClient) { }

  sendMail(msg){
  	const url = this.base_url+'post/feedback';
  	return this.http.post(url,msg,httpOptions).pipe();
  }

}
