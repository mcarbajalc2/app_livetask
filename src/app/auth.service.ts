import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { CookieService } from 'angular2-cookie/core';

import { Router } from '@angular/router';


const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class AuthService {
  private base_url:string = 'https://us-central1-livetask-363bb.cloudfunctions.net/api/auth/'

  constructor(
  	private http: HttpClient,
  	private cookieService: CookieService,
    private router: Router) { };

  logIn(mail: string, pass: string): Observable<any>{
  	let path:string = 'login';
  	let query:string = '?mail='+mail+'&pass='+pass;
  	let url: string = this.base_url + path +query;
  	return this.http.get(url)
  		.pipe();
  }

  verifyAccessToken(){
  	const accessToken = this.cookieService.get('accessToken');
    if(accessToken == undefined){
      this.router.navigate(['login']);
    }
  }

  registerUser(mail: string,pass: string,name: string): Observable<any>{
    let path:string = 'register';
    let url:string = this.base_url+path;
    return this.http.post<any>(url,{mail: mail,pass:pass, name: name},httpOptions).pipe();
  }

  recoveryPassWord(mail: string): Observable<any>{
    let url:string = "https://us-central1-livetask-363bb.cloudfunctions.net/api/recovery";
    return this.http.post<any>(url,{mail: mail});
  }

}
