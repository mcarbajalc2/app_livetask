import { Injectable } from '@angular/core';

import { Folder } from './Models/folder';

import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { CookieService } from 'angular2-cookie/core';

import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class FolderService {
  private base_url:string = 'https://us-central1-livetask-363bb.cloudfunctions.net';
  private get_url:string = this.base_url + '/api/get/folders/';
  private post_url:string = this.base_url + '/api/post/folder/';
  private delete_url:string = this.base_url + '/api/delete/folder/';

  constructor(
  	private http: HttpClient,
  	private cookieService: CookieService,
    private router: Router) { };

  getFolders(mail:string): Observable<Folder[]>{
  	const url = this.get_url + mail;
  	return this.http.get<Folder[]>(url)
  		.pipe();
  }

  addFolder(folder:Folder): Observable<Folder>{
    const url = this.post_url;
    const user_mail = localStorage.getItem("mail");
    console.log(folder);
    return this.http.post<Folder>(url,{folder,user_mail},httpOptions).pipe();
  }

  deleteFolder(folder:Folder): Observable<Folder>{
    const url = this.delete_url;
    const user_mail = localStorage.getItem("mail");
    return this.http.post<Folder>(url,{folder,user_mail},httpOptions).pipe();
  }

}
