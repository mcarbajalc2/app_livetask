import { Injectable } from '@angular/core';
import * as firebase from 'firebase';


@Injectable()
export class FirebaseService {
	private config = {
		apiKey: "AIzaSyCSRIESV0QWel5B6kYsF48dKF6Myo3xcmc",
    authDomain: "livetask-363bb.firebaseapp.com",
    databaseURL: "https://livetask-363bb.firebaseio.com",
    projectId: "livetask-363bb",
    storageBucket: "livetask-363bb.appspot.com",
    messagingSenderId: "376364526338"
	};

	private google_provider = new firebase.auth.GoogleAuthProvider();


  constructor() { 
  	firebase.initializeApp(this.config);
  }

  socialAuth(provider){
  	let socialProvider;
  	switch (provider) {
  		case "google":
  			socialProvider = this.google_provider;
  			break;
  		
  		default:
  			console.log("Invalid provider.");
  			break;
  	}
  	return firebase.auth().signInWithPopup(socialProvider);
  }

}
