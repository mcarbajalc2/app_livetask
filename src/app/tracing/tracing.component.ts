import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Task } from '../Models/task';
import { TaskService } from '../task.service';


@Component({
  selector: 'app-tracing',
  templateUrl: './tracing.component.html',
  styleUrls: ['./tracing.component.scss']
})
export class TracingComponent implements OnInit {
  title:string;
  tasks: Task[];
  user_mail: string;
  selectedType = "edit";
  selectedTask: Task;

  constructor(
  	private route: ActivatedRoute,
  	private titleService: Title,
    private taskService: TaskService) { }

  ngOnInit() {
  	this.title = this.route.snapshot.data.title;
  	this.titleService.setTitle("LiveTask - "+this.title);  	
    this.user_mail = localStorage.getItem('mail');
    this.getTasks();
  }

  getTasks():void{
    this.taskService.getTasks(this.user_mail)
    .subscribe( data => {
      this.tasks = data;
    });
  }

  belognsActive(task:Task){
    if((task.state == 'ACCEPTED' || task.state == 'COMPLETE' || task.state == 'EXPIRATE') && task.parent_id == '0'){
      return true;
    }
    return false;
  }

  belognsUnaccepted(task:Task){
    if((task.state == 'SEND') && task.parent_id == '0'){
      return true;
    }
    return false;
  }

  hasFromName(task:Task){
    if(task.from['name']){
      return true;
    }
    return false;
  }

  getStateClass(task:Task){
    let state_class = "acepted-state";
    switch (task.state) {
      case "ACCEPTED":
        state_class = "acepted-state";
        break;
      
      case "COMPLETE":
        state_class = "complete-state";
        break;

      case "EXPIRATE":
        state_class = "expirate-state";
        break;
    }

    if(task.expiration["activate"] == true){
      const expiration_date = task.expiration["date"];
      const expiration_time = task.expiration["time"];

      const date_array = expiration_date.split("-");
      const year = Number(date_array[0]);
      const month = Number(date_array[1]);
      const day = Number(date_array[2]);

      let hour = Number(expiration_time.substr(0,2));
      let minute = Number(expiration_time.substr(3,2));
      const ampm = expiration_time.substr(-2, 2);

      if(isNaN(minute)){
        minute = Number(expiration_time.substr(3,1));
      }

      if(isNaN(hour)){
        hour = Number(expiration_time.substr(0,1));
        minute = Number(expiration_time.substr(2,2));
        if(isNaN(minute)){
          minute = Number(expiration_time.substr(2,1));
        }
      }                  

      if(ampm == "PM"){
        hour = hour + 12;
      }

      const aux_date = new Date(year,month,day,hour,minute);
      const now_date = new Date();

      if(aux_date <= now_date){
        state_class = "expirate-state";
      }
    }

    return state_class;
  }

  isSonOf(task:Task,sub_task:Task){
    if (task.id == sub_task.parent_id) {
      return true;
    }
  }

  sonTasks(task){
    const sontasks: Task[] = [];
    for(const i in this.tasks){
      if(this.tasks[i].parent_id == task.id ){
        sontasks.push(this.tasks[i]);
      }
    }
    return sontasks;
  }

  selectTasks(task){
    this.selectedTask = task;
  }

  saveNewTask(event): void{
    for(const i in event.tasks){
      if(event.type == 'new'){
        this.tasks.push(event.tasks[i]);
      }
      this.taskService.addTask(event.tasks[i]).subscribe(task_r => {
        console.log(task_r);
      });
    }
  }

  execAction(event):void{
    switch (event.action) {
      case "closeTask":
        this.selectedTask = undefined;
        break;
      
      default:
        // code...
        break;
    }
  }

}
