export class Extension{
	id: number;
	logoImg: string;
	longDesc: string;
	name;
	price: number;
	rating: number;
	shortDesc: string;
	status: number;
	url: string;
	votes: number;
	carousel;
}