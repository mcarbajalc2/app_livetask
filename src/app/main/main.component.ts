import { Component, OnInit} from '@angular/core';
import { AuthService } from '../auth.service';
import { TracingComponent } from '../tracing/tracing.component';
import { LogService } from '../log.service';



@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  title:string;
  log = undefined;
  constructor(
  	private authService: AuthService,
    private logService: LogService) {  	
  	logService.changeEmitted$.subscribe(data => {
      this.log = {
        message: data
      }
      document.getElementsByClassName("message-log")[0].classList.add("animate");
      setTimeout(function(){
        document.getElementsByClassName("message-log")[0].classList.remove("animate");
      },5000);
    });
  }

  ngOnInit() {
  	this.authService.verifyAccessToken();
  }
}
