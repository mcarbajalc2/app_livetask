import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { Profile } from '../Models/profile';
import { LogService } from '../log.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
	profile: Profile;
	selectedImg: any;
	mail: string;
  constructor(
    private pf: ProfileService,
    private ls: LogService
  ){
  	this.mail = localStorage.getItem("mail");
  	pf.getProfile(this.mail).subscribe(data => {
  		this.profile = data;
  	});
  }

  ngOnInit() {
  }

  saveProfile(){
  	this.pf.updateProfile(this.profile).subscribe(response => {
      this.ls.emitChange("Perfil actualizado");
    });
  }

  changeImg(e){
  	const file = e.target.files[0];
  	var reader = new FileReader();
    reader.onload = this.fileOnload;
    reader.readAsDataURL(file);
  }

  fileOnload(e) {
		var result=e.target.result;
		document.getElementsByClassName("profileImg")[0].setAttribute("src",result);
	}

}
