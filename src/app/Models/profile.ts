import { Onboarding } from './onboarding';

interface ProfileData{
	folder:string;
	id:string;
	image:string;
	mail:string;
	name:string;
	onboarding:Onboarding;
	order_by:string;
	screen:string;
	show_complete:number;
	tab:number;
	tipo:string;
}

export class Profile{
	folder:string;
	id:string;
	image:string;
	mail:string;
	name:string;
	onboarding:Onboarding;
	order_by:string;
	screen:string;
	show_complete:number;
	tab:number;
	tipo:string;

	constructor(obj:ProfileData){
		this.folder = obj.folder;
		this.id = obj.id;
		this.mail = obj.mail;
		this.name = obj.name;
		this.onboarding = obj.onboarding;
		this.order_by = obj.order_by;
		this.screen = obj.screen;
		this.show_complete = obj.show_complete;
		this.tab = obj.tab;
		this.tipo = obj.tipo;
	}

	setTipo(tipo:string){
		this.tipo = tipo;
	}
	setTab(tab:number){
		this.tab = tab;
	}
	setShowComplete(show_complete:number){
		this.show_complete = show_complete;
	}
	setScreen(screen:string){
		this.screen = screen;
	}
	setOrderBy(order_by:string){
		this.order_by = order_by;
	}
	setOnboarding(onboarding: Onboarding){
		this.onboarding = onboarding;
	}
	setName(name:string){
		this.name = name;
	}
	setMail(mail:string){
		this.mail = mail;
	}
	setImage(image:string){
		this.image = image;
	}
	setId(id:string){
		this.id = id;
	}
	setFolder(folder:string){
		this.folder = folder;
	}
	getTipo():string{
		return this.tipo;
	}
	getTab():number{
		return  this.tab;
	}
	getShowComplete():number{
		return this.show_complete;
	}
	getScreen():string{
		return this.screen;
	}
	getOrderBy():string{
		return this.order_by;
	}
	getOnboarding():Onboarding{
		return this.onboarding;
	}
	getName():string{
		return this.name;
	}
	getMail():string{
		return this.mail;
	}
	getImage():string{
		return this.image;
	}
	getId():string{
		return this.id;
	}
	getFolder():string{
		return this.folder;
	}
}