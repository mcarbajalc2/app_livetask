import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import { CookieService } from 'angular2-cookie/core';
import { Router } from '@angular/router';

import { FirebaseService } from '../firebase.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  log_data:any = undefined;
  title:string = 'Ingresar';
  register:boolean = false;
  sendText = "Entrar";
  log:any = undefined;

  constructor(
    private authService: AuthService,
    private cookieService: CookieService,
    private router: Router,
    private firebaseService: FirebaseService
  ) { }

  ngOnInit() {
    this.getCokkie("accessToken");
  }

  logIn(mail:any,pass:any): void{
    this.authService.logIn(mail,pass)
      .subscribe(log_data => {
        console.log(log_data);
        this.log_data = log_data;       
        this.setCookie();
      });
  }

  getCokkie(name){
    const accessToken = this.cookieService.get(name);
    console.log(accessToken);
  }

  setCookie(): void{
    if(this.log_data !== undefined){
      const data = this.log_data.user;
      if(data !== undefined){
        if (data.stsTokenManager) {
          localStorage.setItem("mail",data.email);
          localStorage.setItem("accessToken",data.stsTokenManager.accessToken);
          this.cookieService.put('accessToken', data.stsTokenManager.accessToken);
          this.router.navigate(['']);    
          this.log = undefined;
        }else if(data.refreshToken){
          localStorage.setItem("mail",data.email);
          localStorage.setItem("accessToken",data.uid);
          this.cookieService.put('accessToken', data.uid);
          this.router.navigate(['']);    
          this.log = undefined;
        }else{        
          this.setLog(data.code);
        }
      }      
    }    
  }

  setLog(code): void{
    switch (code) {
      case "auth/invalid-email":
        this.log = "E-mail inválido.";
        break;
      case "auth/wrong-password":
        this.log = "Contraseña inválida o no ingresada."
        break;
      case "auth/weak-password":
        this.log = "Contraseña debil";
        break;
      case "auth/email-already-in-use":
        this.log = "E-mail en uso";
        break;
      case "auth/recovery-true":
        this.log = "Se envio un email de validación"
        break;
      case "auth/recovery-false":
        this.log = "Su correo no se encuentra registrado"
        break;
      default:
        // code...
        break;
    }
  }

  registerUser(mail,pass,name){
    this.authService.registerUser(mail,pass,name)
      .subscribe(data => {
        this.log_data = data;
        if(this.log_data.user.uid){
          this.setCookie();
        }else{
          this.setLog(data.code);
        }
      });
  }

  changeFormType(e){
    e.preventDefault();
    var element = e.target;
    if(!element.classList.contains('change-form')){
        element = element.parentNode;
    }
    if(!element.classList.contains('active')){ 
      if (this.register) {
        this.register = false;
        var loginbtn = document.getElementsByClassName('login-btn')[0];
        var registerbtn = document.getElementsByClassName('register-btn')[0];
        loginbtn.classList.add("active");
        registerbtn.classList.remove("active");
      }else{
        this.register = true;
        var loginbtn = document.getElementsByClassName('login-btn')[0];
        var registerbtn = document.getElementsByClassName('register-btn')[0];
        loginbtn.classList.remove("active");
        registerbtn.classList.add("active");
      }
    }
  }

  auth(e,mail,pass,name){    
    e.preventDefault();
    if(this.register){
      this.registerUser(mail,pass,name);
    }else{
      this.logIn(mail,pass);
    }
  }

  recoveryPassword(e){
    e.preventDefault();
    const mail = (<HTMLInputElement> document.getElementById("mail")).value;
    this.authService.recoveryPassWord(mail).subscribe(data => {
       if(data.complete){
         this.setLog("auth/recovery-true");
       }else{
         this.setLog("auth/recovery-false");
       }
    });
  }

  socialSignIn(platform: string){
    this.firebaseService.socialAuth(platform).then(callback => {
      this.log_data = callback;       
      this.setCookie();      
    }).catch(err => {
      console.log("NO OKEY: "+err);
    });
  }

}
