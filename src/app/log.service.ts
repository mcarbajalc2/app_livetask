import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class LogService {
	constructor() { }
	
	private emitChangeSource = new Subject<any>();
	changeEmitted$ = this.emitChangeSource.asObservable();
	
	emitChange(message) {
		this.emitChangeSource.next(message);
	}


}
