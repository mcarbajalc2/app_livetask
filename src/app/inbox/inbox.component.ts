import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TaskService } from '../task.service';
import { FolderService } from '../folder.service';

import { Task } from '../Models/task';
import { Folder } from '../Models/folder';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})

export class InboxComponent implements OnInit {
  title:string;
  tasks: Task[];
  folders: Folder[];
  user_mail: string;

  showSelectDialog: boolean = false;
  toselectTask: Task;
  selectTitle:string = "Folder de destino";

  constructor(
  	private route: ActivatedRoute,
  	private titleService: Title,
    private taskService: TaskService,
    private folderService: FolderService) {
    this.user_mail = localStorage.getItem("mail");
  }

  ngOnInit() {
  	this.title = this.route.snapshot.data.title;
  	this.titleService.setTitle(this.title);
    this.getFolders();
    this.getTasks();
  }

  getTasks(){
    this.taskService.getTasks(this.user_mail).subscribe(data => {
      this.tasks = data;
    });
  }

  getFolders(){
    this.folderService.getFolders(this.user_mail).subscribe(data => {
      this.folders = data;
    });
  }

  belongsHere(task:Task){
    if(task.parent_id == 0 && task.state == 'SEND' && task.dear_mail == this.user_mail){
      return true;
    }
    return false;
  }

  hasFromName(task:Task){
    if(task.from['name']){
      return true;
    }
    return false;
  }

  acceptTask(e,task){
    e.preventDefault();
    this.toselectTask = task;
    this.showSelectDialog = true;
  }

  rejectTask(e,task:Task){
    e.preventDefault();
    this.showSelectDialog = false;
    task.state = 'REJECTED';
    for (const i in this.tasks) {
      if(this.tasks[i].id == task.id){
        this.tasks.splice(+i,1);
        this.updateTask(task);
        break;
      }
    }
  }

  getSelectResponse(event){
    const res = event.response;
    const action = event.action;
    switch (action) {
      case "CONFIRM":
        this.showSelectDialog = false;
        this.toselectTask.folder = res;
        this.toselectTask.state = 'ACCEPTED';
        for (const i in this.tasks) {
          if(this.tasks[i].id == this.toselectTask.id){
            this.tasks.splice(+i,1);
            this.updateTask(this.toselectTask);
            break;
          }
        }
        break;      
      case "CANCEL":
        this.showSelectDialog = false;
        break;
    }    
  }

  updateTask(task){
    this.taskService.addTask(task).subscribe(res => {
      console.log(res);
    });
  }

  haveTasks(){
    let res = false;
    for(let i in this.tasks){
      if(this.belongsHere(this.tasks[i])){
        res = true;
        break;
      }
    }
    return res;
  }
}
