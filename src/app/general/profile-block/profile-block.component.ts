import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../profile.service';
import { Profile } from '../../Models/profile';


@Component({
  selector: 'app-profile-block',
  templateUrl: './profile-block.component.html',
  styleUrls: ['./profile-block.component.scss']
})

export class ProfileBlockComponent implements OnInit {
  profile: Profile;
  user_mail: string = '';

  constructor(
  	private profileService: ProfileService
  ) {
    this.user_mail = localStorage.getItem("mail");    
  }

  ngOnInit() {
  	this.profileService.getProfile(this.user_mail)
  		.subscribe(data => {
  			this.profile = data;
        localStorage.setItem("token",this.profile.id);
  		} );
  }

}
