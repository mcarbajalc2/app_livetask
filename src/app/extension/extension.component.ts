import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { ExtensionService } from '../extension.service';
import { ActivatedRoute } from '@angular/router';
import { UserExtension } from '../Models/user_extension';

@Component({
  selector: 'app-extension',
  templateUrl: './extension.component.html',
  styleUrls: ['./extension.component.scss']
})
export class ExtensionComponent implements OnInit {
  extension: UserExtension;
  constructor(
  	private extensionService:ExtensionService,
  	private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {
  	this.activatedRoute.params.subscribe(params => {
  		const id = params['id'];
      this.extensionService.getUserExtension().subscribe(ext => {
        this.extension = ext[id];
        return false;
      })
  	});
  }

  getSafeURL(url){
    const user_mail = localStorage.getItem("mail");
    return this.sanitizer.bypassSecurityTrustResourceUrl(url+user_mail);
  }

}
