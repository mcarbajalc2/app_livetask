import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges  } from '@angular/core';

import { FolderService } from '../folder.service';
import { TaskService } from '../task.service';

import { Folder } from '../Models/folder';
import { Task } from '../Models/task';
import { LogService } from '../log.service';


@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})

export class TaskListComponent implements OnInit {
  @Input() folders: Folder[];
  @Input() tasks: Task[] = [];
  @Input() complete: number = 1;
  @Input() orderBy: number = 4;

  @Output() passSelectedTask = new EventEmitter();

  user_mail: string = '';
  active_folder: Folder;
  new_task_name: string = '';
  new_folder_name: string = '';

  constructor(
  	private folderService: FolderService,
    private taskService: TaskService,
    private logService: LogService    
  ) {
    this.user_mail = localStorage.getItem("mail");
  }

  ngOnChanges(changes: SimpleChanges){
    if(changes.folders){
      this.targetActiveFolder();
    }
    if (changes.orderBy) {
      this.sortTasks();
    }
    if(changes.tasks){
      console.log("x");
      this.sortTasks();
    }
  }

  ngOnInit() {
    console.log(this.orderBy);
    this.sortTasks();
  }

  sortTasks(): void{
    if (this.tasks !== undefined) {
      switch (this.orderBy) {
        case 0:
          this.tasks.sort((a,b) => {
            if (a.name < b.name) {
              return -1;
            }else if (a.name > b.name) {
              return 1;
            }
            return 0;
          });
          break;
        case 1:
          this.tasks.sort((a,b) => {
            const a_date = new Date(a.created["time"]);
            const b_date = new Date(b.created["time"]);
            if (a_date < b_date) {
              return -1;
            }else if (a_date > b_date) {
              return 1;
            }
            return 0;
          });
          break;
        case 2:
          this.tasks.sort((a,b) => {
            const a_date = new Date(a.updated["time"]);
            const b_date = new Date(b.updated["time"]);
            if (a_date < b_date) {
              return -1;
            }else if (a_date > b_date) {
              return 1;
            }
            return 0;
          });
          break;
        case 3:
          this.tasks.sort((a,b) => {
            if (a.favorite > b.favorite) {
              return -1;
            }else if(a.favorite < b.favorite){
              return 1;
            }
            return 0;
          });
          break;
        default:
          // code...
          break;
      }
    }    
  }

  targetActiveFolder():void{
    for(const i in this.folders){
      if(this.folders[i].selected){
        this.active_folder = this.folders[i];
        return;
      }
    }
  }

  taskBelongsHere(folder,task):boolean{
    let ok = false;
    if(task.folder == folder.name && task.parent_id == 0){
      switch (task.state) {
        case "ACCEPTED":
          ok = true;
          break;
        case "COMPLETE":
          ok = true;
          break;
        case "EXPIRATE":
          ok = true;
          break;
        case "NO_SEND":
          ok = true;
          break;
        default:
          ok = false;
          break;
      }
    }
    return ok;
  }

  changeFolder(folder){
    this.active_folder = folder;
  }

  showTask(task:Task): void{    
    this.passSelectedTask.emit({task: task,type: 'edit'});    
  }

  addTask(e): void{
    e.preventDefault();
    if ((this.new_task_name).trim() == '') {
      this.addTaskFull();
    }else{
      this.addTaskFast();
    }    
  }

  addFolder(e):void{
    e.preventDefault();
    if ((this.new_folder_name).trim() == '') {
      console.log("Ingrese un nombre");
    }else{
      const key = this.folders.length;
      const folder:Folder = {id:key, key:key, name: this.new_folder_name, selected: false} as Folder;
      this.folders.push(folder);
      this.folderService.addFolder(folder).subscribe(folder_r => {
        console.log("Folder Added");
      });
    }  
  }

  removeFolder(e,folder){
      for(const i in this.folders){
        if (this.folders[i].id == folder.id) {
          this.folders.splice(+i,1);
          this.folderService.deleteFolder(folder).subscribe(folder_r => {
            console.log("Folder removed");
          });
          return;
        }
      }      
  }

  addTaskFull(){
    const date = new Date();
    const alert = { active: false }
    const created = {
      date: date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate(),
      millisecond: date.getMilliseconds(),
      time: date.getTime(),
    };
    const folder = this.active_folder.name;
    const dear = {
      mail: localStorage.getItem("mail")
    };
    const dear_mail = localStorage.getItem("mail");;
    const description = '';
    const expiration = {
      active: false
    };
    const favorite = 0;
    const from = {
      mail: localStorage.getItem("mail")
    };
    const from_mail = localStorage.getItem("mail");
    const id = localStorage.getItem("token")+'__'+Date.now();
    const key = id;
    const name = 'Nueva Tarea';
    const parent_id = 0;
    const state = 'ACCEPTED';
    const updated = created;
    const visible = true;

    const task = {alert,created,folder,dear,dear_mail,description,expiration,favorite,from,from_mail,id,key,name,parent_id,state,updated,visible} as Task;
    this.passSelectedTask.emit({task: task,type:'new'});
  }

  setFavorite(task:Task){
    if(task.favorite == 1){
     task.favorite = 0;
    }else{
      task.favorite = 1;
    }
    this.taskService.addTask(task).subscribe(task_r => {
      for (const i in this.tasks) {
        if(this.tasks[i].key == task.key){
          this.tasks[i] = task;
        }
      }
      console.log(this.tasks);
    });
    
  }

  deleteTask(e,task:Task){
    task.state = 'DELETED';
    this.taskService.addTask(task).subscribe(task_r => {
      for (const i in this.tasks) {
        if(this.tasks[i].key == task.key){
          this.tasks[i] = task;
        }
      }      
    });

  }

  completeTask(e,task:Task){
    task.state = 'COMPLETE';
    this.taskService.addTask(task).subscribe(task_r => {
      for (const i in this.tasks) {
        if(this.tasks[i].key == task.key){
          this.tasks[i] = task;
          this.logService.emitChange("Tarea completada");
          break;
        }
      }
    });
  }

  folderHasTasks(folder){
    let response = false;
    for(let i in this.tasks){
      if (this.taskBelongsHere(folder,this.tasks[i])) {
        response = true;
      }
    }
    return response;
  }

  addTaskFast(){
    const date = new Date();
    const alert = { active: false }
    const created = {
      date: date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate(),
      millisecond: date.getMilliseconds(),
      time: date.getTime(),
    };
    const folder = this.active_folder.name;
    const dear = {
      mail: localStorage.getItem("mail")
    };
    const dear_mail = localStorage.getItem("mail");;
    const description = '';
    const expiration = {
      active: false
    };
    const favorite = 0;
    const from = {
      mail: localStorage.getItem("mail")
    };
    const from_mail = localStorage.getItem("mail");
    const id = localStorage.getItem("token")+'__'+Date.now();
    const key = id;
    const name = this.new_task_name;;
    const parent_id = 0;
    const state = 'ACCEPTED';
    const updated = created;
    const visible = true;

    const task = {alert,created,folder,dear,dear_mail,description,expiration,favorite,from,from_mail,id,key,name,parent_id,state,updated,visible} as Task;
    this.tasks.push(task);
    this.taskService.addTask(task).subscribe(task_r => {
      this.logService.emitChange("Tarea Creada");
    });
  }

}
