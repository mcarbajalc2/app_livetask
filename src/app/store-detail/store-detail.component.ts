import { Component, OnInit, Input } from '@angular/core';
import { Extension } from '../Models/extension';
import { ExtensionService } from '../extension.service';
import { UserExtension } from '../Models/user_extension';



@Component({
  selector: 'app-store-detail',
  templateUrl: './store-detail.component.html',
  styleUrls: ['./store-detail.component.scss']
})
export class StoreDetailComponent implements OnInit {
  @Input() extension: Extension;
  @Input() userExtensions: UserExtension[];
  isInstalled:boolean = true;
  prueba="asdxx";
  constructor(
  	private extensionService: ExtensionService
  ){ }

  ngOnInit() {
  	console.log(this.extension);
    console.log(this.userExtensions);
    this.verifyInstalled();
  }

  registerUser(){
  	this.extensionService.setUserExtension(this.extension).subscribe( response => {
  		console.log(response);
  	});
  }

  verifyInstalled(){
    for (const i in this.userExtensions) {
      if(this.userExtensions[i] !== null && this.userExtensions[i].id == this.extension.id){
        this.isInstalled = false;
        break;
      }
    }
  }

  unregisterUser(){
    
  }

}
