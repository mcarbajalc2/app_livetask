import { Component, OnInit } from '@angular/core';
import { MiscService } from '../misc.service';
import { LogService } from '../log.service';


@Component({
  selector: 'app-feed-back',
  templateUrl: './feed-back.component.html',
  styleUrls: ['./feed-back.component.scss']
})
export class FeedBackComponent implements OnInit {
	asunto: string = '';
	telefono: string = '';
	pais: string = '';
	mensaje: string = '';
  stop: boolean = false;

  constructor(
    private ms: MiscService,
    private ls: LogService
  ) { }

  ngOnInit() {
  }

  enviarFormulario(){
    const msg = {
       asunto: this.asunto,
       telefono: this.telefono,
       pais: this.pais,
       mensaje: this.mensaje,
       user_mail: localStorage.getItem('mail')
    };
    this.stop = true;
    this.ls.emitChange("Enviando Mensaje");
    this.ms.sendMail(msg).subscribe(res => {
      if(res['complete']){
        this.asunto = '';
        this.telefono = '';
        this.pais = '';
        this.mensaje = '';        
        this.ls.emitChange("Mensaje enviado");        
      }
      console.log(res);
      this.stop = false;
    });
  }

  cleanFields(){
    this.asunto = "";
    this.telefono = "";
    this.pais = "";
    this.mensaje = "";
  }

}
