import { Injectable } from '@angular/core';

import { Profile } from './Models/profile';


import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { CookieService } from 'angular2-cookie/core';

import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class ProfileService {
  private base_url:string = 'https://us-central1-livetask-363bb.cloudfunctions.net/api/';
  private get_url:string = this.base_url+"get/profile/";
  private post_url:string = this.base_url+"post/profile/";

  constructor(
  	private http: HttpClient,
  	private cookieService: CookieService,
    private router: Router) { };

  getProfile(mail:string): Observable<Profile>{
  	const url = this.get_url + mail;
  	return this.http.get<Profile>(url)
  		.pipe();
  }

  updateProfile(profile:Profile){
    const url = this.post_url+profile.mail;
    return this.http.post<Profile>(url,profile,httpOptions).pipe();
  }

}
