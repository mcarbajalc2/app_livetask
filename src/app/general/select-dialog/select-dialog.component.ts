import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-select-dialog',
  templateUrl: './select-dialog.component.html',
  styleUrls: ['./select-dialog.component.scss']
})
export class SelectDialogComponent implements OnInit {
  @Input() title: string;
  @Input() options;
  selected;

  @Output() response = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.setSelected();
  }

  sendResponse(action){
  	this.response.emit({
  		response: this.selected,
      action: action
  	});
  }

  setSelected(){
    for (const i in this.options) {
       if(this.options[i].selected){
         this.selected = this.options[i].name;
         break;
       }
    }
  }

}
