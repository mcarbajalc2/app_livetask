import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../Models/task';
import { NgModel } from '@angular/forms';
import { DateTimeAdapter } from 'ng-pick-datetime';


declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss']
})
export class TaskDetailComponent implements OnInit {
  @Input() task:Task;
  @Input() tasks:Task[];
  @Input() modo:string;
  @Input() type:string;

  @Output() passAction = new EventEmitter();
  @Output() saveNewTask = new EventEmitter();

  task_expiration_date;
  task_expiration_label:string = "Vencimiento";

  task_alert_date;  
  task_alert_label:string = "Alerta";

  min = new Date();

  sub_task_name:string = '';

  constructor(
    dateTimeAdapter: DateTimeAdapter<any>) {
    dateTimeAdapter.setLocale('es');
  }

  ngOnInit() {}

  ngAfterViewInit(){
    this.showTaskDates();
    this.tasks.push(this.task);
  }

  turnBack(event) {
  	if(this.task.parent_id == 0){
  		this.passAction.emit({action: "closeTask"});
      let task_list = document.getElementsByTagName("app-task-list")[0];
      if (task_list.classList.contains('d-none')) {
        task_list.classList.add('d-flex');
        task_list.classList.remove('d-none');
      }
  	}else{
      for(const i in this.tasks){
        if(this.tasks[i].id == this.task.parent_id){
          this.task = this.tasks[i];
          this.showTaskDates();
          break;
        }
      }
    }	
  }

  sendTask(event){
  	if(this.task.parent_id !== 0){
      for(const i in this.tasks){
        if(this.tasks[i].id == this.task.parent_id){
          this.task = this.tasks[i];
          this.showTaskDates();
          break;
        }
      }
    }else{
      if(this.task.dear_mail !== this.task.from_mail){
        this.task.state = 'SEND';
      }
      this.saveNewTask.emit({tasks: this.tasks,type: this.type});
    }
  }

  closeExpirationPicker(event){
    if(this.task_expiration_date == undefined){
      this.task_expiration_label = "Vencimiento";
    }else{
      console.log(event);
      this.task_expiration_label = "FECHA VENCIMIENTO";
    }
  }

  expirationTimeChange(event){
    const fecha:Date = this.task_expiration_date;
    const year = fecha.getFullYear();

    let month = '';
    if((fecha.getMonth()+1) < 10){
      month = "0"+(fecha.getMonth()+1) 
    }else{
      month = ""+(fecha.getMonth()+1);
    }

    let day = "";
    if(fecha.getDate() < 10){
      day = "0"+fecha.getDate();
    }else{
      day = ""+fecha.getDate();
    }

    let hours = '';
    if(fecha.getHours() < 10){
      hours = "0"+fecha.getHours();
    }else{
      hours = ""+fecha.getHours();
    }

    let minutes = '';
    if(fecha.getMinutes() < 10){
      minutes = "0"+fecha.getMinutes();
    }else{
      minutes = ""+fecha.getMinutes();
    }

    const millisecond = fecha.getMilliseconds();
    console.log(hours);
    console.log(minutes);
    const time = this.getFormat12Time(hours+':'+minutes);    
    const expiration = {
      activate: true,
      date: year+'-'+(month)+'-'+day,
      time: time
    }
    this.task_expiration_label = day+'-'+(month)+'-'+year+' '+time;
    this.task.expiration = expiration;
  }

  getFormat12Time(time){
    console.log(time);
    let timeString = time;
    const H = +timeString.substr(0, 2);
    const h = H % 12 || 12;
    const ampm = (H < 12 || H === 24) ? " AM" : " PM";
    timeString = h + timeString.substr(2, 3) + ampm;
    return timeString;
  }

  getFormat24Time(time){
    let H = +time.substr(0, 2);
    const M = +time.substr(2, 2);
    const ampm = time.substr(-2, 2);
    if(ampm == 'PM'){
      H = H+12;
    }
    return ampm;
  }

  alertTimeChange(event){
    const fecha:Date = this.task_alert_date;
    const year = fecha.getFullYear();

    let month = '';
    if((fecha.getMonth()+1) < 10){
      month = "0"+(fecha.getMonth()+1) 
    }else{
      month = ""+(fecha.getMonth()+1);
    }

    let day = "";
    if(fecha.getDate() < 10){
      day = "0"+fecha.getDate();
    }else{
      day = ""+fecha.getDate();
    }

    let hours = '';
    if(fecha.getHours() < 10){
      hours = "0"+fecha.getHours();
    }else{
      hours = ""+fecha.getHours();
    }

    let minutes = '';
    if(fecha.getMinutes() < 10){
      minutes = "0"+fecha.getMinutes();
    }else{
      minutes = ""+fecha.getMinutes();
    }

    const millisecond = fecha.getMilliseconds();
    const time = this.getFormat12Time(hours+':'+minutes);
    const alert = {
      activate: true,
      date: year+'-'+(month)+'-'+day,
      time: time
    }
    this.task_alert_label = day+'-'+(month)+'-'+year+' '+time;
    this.task.alert = alert;
  }

  closeAlertPicker(event){
    if(this.task_alert_date == undefined){
      this.task_alert_label = "Alerta";
    }else{
      console.log(event);
      this.task_alert_label = "FECHA ALERTA";
    }
  }

  isSon(task):boolean{
    if(this.task.id == task.parent_id){
      return true;
    }
    return false;
  }

  addSubTask(event){
    event.preventDefault();
    if(this.sub_task_name !== ''){
      const date = new Date();
      const alert = { activate: false }
      const created = {
        date: date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate(),
        millisecond: date.getMilliseconds(),
        time: date.getTime(),
      };
      const folder = this.task.folder;
      const dear = this.task.dear;
      const dear_mail = this.task.dear_mail;
      const description = '';
      const expiration = {
        activate: false
      };
      const favorite = 0;
      const from = {
        mail: localStorage.getItem("mail")
      };
      const from_mail = localStorage.getItem("mail");
      const id = localStorage.getItem("token")+'__'+Date.now();
      const key = id;
      const name = this.sub_task_name;
      const parent_id = this.task.id;
      const state = 'ACCEPTED';
      const updated = created;
      const visible = true;

      const task = {alert,created,folder,dear,dear_mail,description,expiration,favorite,from,from_mail,id,key,name,parent_id,state,updated,visible} as Task;
      this.tasks.push(task);
    }else{
      console.log("Ingrese un nombre a la tarea");
    }
  }

  showSubTask(s_task){
    this.task = s_task;
    console.log(this.task);
    this.showTaskDates();
  }

  showTaskDates(){
    if(this.task.expiration['activate'] == true){      
      const date_array = (this.task.expiration['date']).split("-");
      const time = this.getFormat24Time(this.task.expiration['time']);
      this.task_expiration_date = new Date(date_array[0],date_array[1],date_array[2]);
      this.task_expiration_label = this.task.expiration['date']+' - '+this.task.expiration['time'];      
    }else{
      this.task_expiration_date = undefined;
      this.task_expiration_label = 'Vencimiento';
      console.log("no expiracion");   
    }

    if(this.task.alert['activate'] == true){      
      const date_array = (this.task.alert['date']).split("-");
      const time = this.getFormat24Time(this.task.alert['time']);
      this.task_alert_date = new Date(date_array[0],date_array[1],date_array[2]);
      this.task_alert_label = this.task.alert['date']+' - '+this.task.alert['time'];      
    }else{
      this.task_alert_date = undefined;
      this.task_alert_label = 'Alerta';
      console.log("no expiracion");      
    }
  }
  removeSubTask(s_task){
    s_task.state = 'DELETED';
    for (let i in this.tasks) {
      if(this.tasks[i].id == s_task.id){
        this.tasks[i].id = s_task;
        break;
      }
    }
    console.log("eliminar sub task");
  }
  isActive(s_task){
    if(s_task.state == 'DELETED'){
      return false;
    }else{
      return true;
    }
  }
}
