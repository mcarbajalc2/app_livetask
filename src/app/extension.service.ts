import { Injectable } from '@angular/core';

import { Extension } from './Models/extension';
import { UserExtension } from './Models/user_extension';

import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { CookieService } from 'angular2-cookie/core';

import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class ExtensionService {
  private base_url:string = 'https://us-central1-livetask-363bb.cloudfunctions.net';
  private get_url:string = this.base_url + '/api/get/apps';
  private get_url_user_extension = this.base_url + '/api/get/user_extensions/';;
  private post_url:string = this.base_url + '/api/post/user_app/';

  constructor(
  	private http: HttpClient,
  	private cookieService: CookieService,
    private router: Router) { }

  getApps(): Observable<Extension[]>{
  	const url = this.get_url;
  	return this.http.get<Extension[]>(url)
  		.pipe();
  }

  setUserExtension(extension:Extension): Observable<UserExtension>{
    const url = this.post_url;
    const user_token = localStorage.getItem("token");
    return this.http.post<UserExtension>(url,{extension,user_token},httpOptions).pipe();
  }

  getUserExtension():Observable<UserExtension[]>{
    const user_token = localStorage.getItem("token");
    const url = this.get_url_user_extension + user_token;
    return this.http.get<UserExtension[]>(url).pipe();
  }

}
