import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { MainComponent  } from './main/main.component';
import { MyTasksComponent } from './my-tasks/my-tasks.component';
import { InboxComponent } from './inbox/inbox.component';
import { TracingComponent } from './tracing/tracing.component';
import { StoreComponent } from './store/store.component';
import { ExtensionComponent } from './extension/extension.component';
import { FeedBackComponent } from './feed-back/feed-back.component';
import { ProfileComponent } from './profile/profile.component';


const routes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: '', component: MainComponent,
	  children: [
	  	{ path: '', component: MyTasksComponent, data: {title: 'Mis Tareas'} },
	  	{ path: 'inbox', component: InboxComponent, data: {title: 'Bandeja de Entrada'} },
	  	{ path: 'tracing', component: TracingComponent, data: {title: 'Seguimiento'} },
	  	{ path: 'store', component: StoreComponent, data: {title: 'Extensiones'} },
	  	{ path: 'profile', component: ProfileComponent, data: {title: 'Perfiles'} },
	  	{ path: 'extension/:id', component: ExtensionComponent },
	  	{ path: 'feedback', component: FeedBackComponent }
	  ]
	}
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})

export class AppRoutingModule { }
