import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Extension } from '../Models/extension';
import { ExtensionService } from '../extension.service';

import { UserExtension } from '../Models/user_extension';


@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {
  title:string;
  extensions: Extension[];
  selectedExtension:Extension = undefined;
  user_extensions: UserExtension[];
  constructor(
  	private route: ActivatedRoute,
  	private titleService: Title,
    private extensionService: ExtensionService) { }

  ngOnInit() {
  	this.title = this.route.snapshot.data.title;
    this.getExtensions();
    this.getUserExtensions();
  }

  getExtensions(){
    this.extensionService.getApps().subscribe(data => {
      this.extensions = data;
    });
  }

  isFree(ext: Extension){
    if(ext.price == 0){
      return true;
    }
    return false;
  }
  showAppDetail(ext: Extension){
    console.log(ext);
    this.selectedExtension = ext;
  }
  toBack(){
    this.selectedExtension = undefined;
  }
  getUserExtensions(){
    this.extensionService.getUserExtension().subscribe(data => {
      this.user_extensions = data;
    });
  }

}
