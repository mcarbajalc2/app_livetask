import { Injectable } from '@angular/core';

import { Task } from './Models/task';

import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { CookieService } from 'angular2-cookie/core';

import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class TaskService {
  private base_url:string = 'https://us-central1-livetask-363bb.cloudfunctions.net';
  private get_url:string = this.base_url + '/api/get/tasks/';
  private post_url:string = this.base_url + '/api/post/task';
  constructor(
  	private http: HttpClient,
  	private cookieService: CookieService,
    private router: Router
  ) { }

  getTasks(user_mail:string):Observable<Task[]>{
  	const url = this.get_url + user_mail
  	return this.http.get<Task[]>(url).pipe();
  }

  addTask(task:Task): Observable<Task>{
    const url = this.post_url;
    return this.http.post<Task>(url,{task},httpOptions).pipe();
  }
}
