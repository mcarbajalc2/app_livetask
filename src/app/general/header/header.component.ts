import { Component, OnInit } from '@angular/core';
import { CookieService } from 'angular2-cookie/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
  	private cookieService: CookieService,
  	private router: Router) { }

  ngOnInit() {
  }

  closeSession(): void{
  	this.cookieService.removeAll();
  	location.reload();
  }

}
