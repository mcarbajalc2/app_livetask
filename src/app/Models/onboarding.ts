interface OnboardingData{
	add_complete_task: boolean;
	add_task: boolean;
	complete_task:boolean;
	detail_from: boolean;
	detail_name: boolean;
}
export class Onboarding {
	private add_complete_task: boolean;
	private add_task: boolean;
	private complete_task:boolean;
	private detail_from: boolean;
	private detail_name: boolean;
	
	constructor(obj: OnboardingData) {
		this.add_complete_task = obj.add_complete_task;
		this.add_task = obj.add_task;
		this.complete_task = obj.complete_task;
		this.detail_from = obj.detail_from;
		this.detail_name = obj.detail_name;
	}
	setDetailName(detail_name:boolean){
		this.detail_name = detail_name;
	}
	setDetailFrom(detail_from:boolean){
		this.detail_from = detail_from;
	}
	setCompleteTask(complete_task:boolean){
		this.complete_task = complete_task;
	}
	setAddTask(add_task){
		this.add_task = add_task;
	}
	setAddCompleteTask(add_complete_task:boolean){
		this.add_complete_task = add_complete_task;
	}
	getDetailName():boolean{
		return this.detail_name;
	}
	getDetailFrom():boolean{
		return this.detail_from;
	}
	getCompleteTask():boolean{
		return this.complete_task;
	}
	getAddTask():boolean{
		return this.add_task;
	}
	getAddCompleteTask():boolean{
		return this.add_complete_task;
	}
}