import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { FolderService } from '../folder.service';
import { TaskService } from '../task.service';
import { LogService } from '../log.service';

import { Folder } from '../Models/folder';
import { Task } from '../Models/task';

@Component({
  selector: 'app-my-tasks',
  templateUrl: './my-tasks.component.html',
  styleUrls: ['./my-tasks.component.scss']
})
export class MyTasksComponent implements OnInit {
  title:string;
  folders: Folder[];
  tasks: Task[];
  user_mail: string = '';
  selectedTask: Task;
  selectedTaskSons: Task[];
  orderBy: number = 4;
  complete: number = 0;
  selectedType: string = "";

  constructor(
  	private route: ActivatedRoute,
  	private titleService: Title,
    private folderService: FolderService,
    private taskService: TaskService,
    private logService: LogService) {    
    this.user_mail = localStorage.getItem("mail");
  }

  ngOnInit() {
    this.title = this.route.snapshot.data.title;
    this.getFolders();
    this.getTasks();
  }

  getFolders():void{
    this.folderService.getFolders(this.user_mail)
      .subscribe(data => {
        this.folders = data;
      });
  }

  getTasks():void{
    this.taskService.getTasks(this.user_mail)
    .subscribe( data => {
      this.tasks = data;
    });
  }

  showTask(event):void{
    this.selectedTask = event.task;
    this.selectedType = event.type;
    this.selectedTaskSons = this.getSelectedSons(this.selectedTask.id);
    let task_list = document.getElementsByTagName("app-task-list")[0];
    if (!task_list.classList.contains('d-none')) {
      task_list.classList.add('d-none');
      task_list.classList.remove('d-flex');
    }
  }

  showTaskList(event):void{
    this.tasks = event.tasks;
    console.log(this.tasks);
  }

  getSelectedSons(task_id): Task[]{
    const sons:Task[] = [];
    for(const i in this.tasks){      
      if(this.tasks[i].parent_id == task_id){
        sons.push(this.tasks[i]);
      }
    }
    return sons;
  }

  execAction(event):void{
    switch (event.action) {
      case "closeTask":
        this.selectedTask = undefined;
        break;
      
      default:
        // code...
        break;
    }
  }

  saveNewTask(event): void{
    for(const i in event.tasks){
      if(event.type == 'new'){
        this.tasks.push(event.tasks[i]);
      }
      this.taskService.addTask(event.tasks[i]).subscribe(task_r => {
        if(task_r.dear_mail == task_r.from_mail){
          this.logService.emitChange("Tarea Creada");
        }else{
          this.logService.emitChange("Tarea Enviada");
        }        
      });
    }
  }

  changeOrderByState(event,val):void{
    var element = event.target;    
    var list = document.getElementsByClassName("mc2-label");
    Array.from(list).forEach( (elementx,index,array) => {
      var check = <Element>elementx.lastChild;
      //(check).classList.remove("d-none");
      check.classList.add("d-none");
    });
    if (element.classList.contains("mc2-label")) {
      element = element.lastChild;
    }
    this.orderBy = val;
    element.classList.remove("d-none");
  }

  showCompleted(event,val):void{
    var element = event.target
    var list = document.getElementsByClassName("mc2-label-completed");
    Array.from(list).forEach( (elementx,index,array) => {
      var check = <Element>elementx.lastChild;
      //(check).classList.remove("d-none");
      check.classList.add("d-none");
    });
    if (element.classList.contains("mc2-label-completed")) {
      element = element.lastChild;
    }
    this.complete = val;
    element.classList.remove("d-none");
  }

}
